from lib2to3.pytree import Base
from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel
import uvicorn

app = FastAPI()


@app.get("/")
def index(limit = 10, published: bool = True, sort : Optional[str]= None):
    if published:
        return {"data": f"{limit} published blogs"}
    else:
        return {"data": f"{limit} blog list"}

@app.get("/blog/unpublished")
def unpublished():
    return {"data": "list of all unpublished blogs"}

@app.get("/blog/{id}")
def show_blog(id: int): #int is type hinting
    return {"data": id}

@app.get("/blog/{id}/comments")
def show_comments(id: int, limit = 10): #here id is path parameter & limit is query parameter
    return {"data": {"1", "2"}}

class Blog(BaseModel):
    pass

@app.post("/blog/")
def create_blog(request: Blog):
    return {"data": "Blog is created"}


# if __name__ == "__main__":
#     uvicorn.run(app, host="127.0.0.1", port=8088)