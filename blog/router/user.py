import imp
from fastapi import APIRouter, FastAPI, Depends, HTTPException,status
from ..db import get_db
from .. import schema,model
from ..repository import user
from ..db import get_db,SessionLocal

router = APIRouter(
    tags=["User"],
    prefix="/user"
)

@router.post("/", response_model=schema.ShowUser)
def create_user(request: schema.User, db: SessionLocal=Depends(get_db)):
    return user.create(db, request)
    
@router.get("/{id}", response_model= schema.ShowUser)
def get_user(id: int, db: SessionLocal=Depends(get_db)):
   return user.get_user(id, db)