from fastapi import APIRouter, FastAPI, status,Response, Depends, HTTPException
from sqlalchemy.orm import Session

from blog.oauth import get_current_user
from .. import model,schema,oauth
from ..db import get_db,SessionLocal
from typing import List
from ..repository import blog


router = APIRouter(
    tags=["Blog"],
    prefix="/blog"
)

@router.post("/", status_code=status.HTTP_201_CREATED)
def create(request: schema.Blog, db: Session = Depends(get_db), current_user: schema.User = Depends(oauth.get_current_user)):
    return blog.create(request, db)

@router.get("/", response_model=List[schema.ShowBlog])
def get_blogs(db: SessionLocal = Depends(get_db), current_user: schema.User = Depends(oauth.get_current_user)):
    return blog.get_blogs(db)


@router.get("/{id}", response_model=schema.ShowBlog)
def get_blog(id: int, response: Response, db: SessionLocal = Depends(get_db), current_user: schema.User = Depends(oauth.get_current_user)):
    return blog.get_blog_by_id(db, id) 

@router.delete("/{id}")
def delete(id: int, response: Response, db : SessionLocal=Depends(get_db), current_user: schema.User = Depends(oauth.get_current_user)):
    return blog.delete(db, id)
        

@router.put("/{id}")
def update(id: int, request: schema.Blog, db: SessionLocal = Depends(get_db), current_user: schema.User = Depends(oauth.get_current_user)):
    return blog.update(db, id, request)