from fastapi import FastAPI
from . import schema, model
from .db import  engine
from .router import blog,user,auth


app = FastAPI()

model.Base.metadata.create_all(bind=engine)

app.include_router(blog.router)
app.include_router(user.router)
app.include_router(auth.router)