from fastapi import HTTPException, status
from .. import model,schema
from ..hashing import Hash
from sqlalchemy.orm import Session

def create(db: Session, request:schema.User):
    newUser = model.User(name= request.name, email= request.email, password= Hash.getHash(request.password))
    db.add(newUser)
    db.commit()
    db.refresh(newUser)
    return newUser

def get_user(id:int, db: Session):
    user = db.query(model.User).filter(model.User.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found!")
    return user