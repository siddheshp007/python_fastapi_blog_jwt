from fastapi import HTTPException, status
from .. import model, schema
from sqlalchemy.orm import Session

def get_blogs(db):
    blogs = db.query(model.Blog).all()
    return blogs

def create(request: schema.Blog, db: Session):
    newBlog = model.Blog(title= request.title, body= request.body, user_id= 1)
    db.add(newBlog)
    db.commit()
    db.refresh(newBlog)
    return newBlog

def get_blog_by_id(db: Session, id: int):
    blog = db.query(model.Blog).filter(model.Blog.id == id).first()
    if not blog:
        # response.status_code = status.HTTP_404_NOT_FOUND
        # return {"data" : f"blog id {id} not found!"}
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail = f"blog id {id} not found!")
    return blog

def update(db: Session,id:int,  request: schema.Blog):
    # db.query(model.Blog).filter(model.Blog.id == id).update({'title': blog.title, 'body': blog.body}, synchronize_session = False)
    blogData = db.query(model.Blog).filter(model.Blog.id == id)
    if not blogData.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Blog id {id} not found")

    blogData.update(request)
    db.commit()
    return 'Blog updated'

def delete(db:Session, id:int):
    blog = db.query(model.Blog).filter(model.Blog.id == id)
    if not blog.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Blog Not found!")

    blog.delete(synchronize_session = False)
    db.commit()
    return 'Blog deleted'