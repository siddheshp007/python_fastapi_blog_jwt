# python_fastApi

- [ ]  [Click here to explore episodes](https://www.youtube.com/watch?v=Fzdn_ZovZUY&list=PL5gdMNl42qynpY-o43Jk3evfxEKSts3HS&index=2)
- [ ]  [Click here to explore episodes](https://www.youtube.com/watch?v=G7hZlOLhhMY)

## Introduction to fast API

- [ ] A modern, fast(high performance), web framework for building APIs with python 3.6+
- [ ] Based on standard python type hint
- [ ] Desgined around OneApi(Swagger) and Json schema standards.
- [ ] Security & authentication integrated.

## Features

- [ ] It is fast as compared to some other frameworks
- [ ] Fast to code
- [ ] Fewer bugs
- [ ] Easy

## FastApi supports following starlette(framework) features
- [ ] Websocket support
- [ ] GraphQl support
- [ ] in process background tasks


## Pydentic models
- [ ] When you need to send data from client to you API, you need to send it as request body. To declare a request body, you have to use pydantic models.
````
from pydantic import BaseModel

class Item(BaseModel):
	name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None

@app.post("/items/")
async def create_item(item: Item):
    return item
````

## Uvicorn
- [ ] Uvicorn is an ASGI (Asynchronous Server Gateway Interface) web server implementation for Python.
- [ ] Until recently Python has lacked a minimal low-level server/application interface for async frameworks. The ASGI specification fills this gap, and means we're now able to start building a common set of tooling usable across all async frameworks.

## Difference between ASGI & WSGI (https://www.youtube.com/watch?v=LtpJup6vcS4)
- [ ] WSGI (Web Server Gateway Interface) used in django & flask frameworks, ASGI is used in fastapi
- [ ] WSGI performs task based on workers, but there is a limit on number of workers. i.e. 2 X(Number of CPU cores) + 1.
- [ ] ASGI performs asynchronous tasks, so the tasks executes simultanenously.
- [ ] ASGI handles lot more request than WSGI.

 
## Create virtual environment (venv)

- [ ] Go to bottom right of pycharm and click on project name. it will show popup for "add interpreter", click it.
- [ ] Select Virtualenv Environment, and then select existing environment. Check your project name is present there or not.
- [ ] It will create venv folder with all required packages
- [ ] Or else you can simply install using "python3 -m venv <venv>"

## Install required packages
- [ ] Create a file "requirements.txt" on root level.
- [ ] Open that file in the pycharm, it will show "install requirements" on top right corner of pycharm. Click on that.
- [ ] It will install all dependencies mentioned in "requirements.txt" file.

## Project set up
````
> mkdir <projectName>
> cd <projectName>

#create virtual environment
> python3 -m venv venv

#activate venv
> source venv/bin/activate

#install required pacakages 
>pip3 install fastapi uvicorn pymongo pymongo[srv] 

OR
#you can install using requirements.txt
> pip3 install -r requirements.txt
````
## How to run project
 - [ ] Create main.py file at root level.
````
import uvicorn
from fastapi import FastAPI

#below app variable is used when we run project, i.e. uvicorn main:<app> --reload
```
app = FastAPI()

#route code starts here

@app.get("/")                           #decorator for routes
async def root():                       #we can define function name as per requirements
    return {"message": "Hello World"}
#route code ends here

if __name__ == "__main__":
    uvicorn.run(app, host = "127.0.0.1", port= 8000)   
````
 - [ ] To run project : uvicorn main:app --reload 
```
 uvicorn main:app --reload
```
 - [ ] It will create server with host:127.0.0.1:8000.
 - [ ] You can check swagger documentation using url: http://127.0.0.1:8000/docs
 
## How to change port
```
import uvicorn

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8088)

```
- [ ] run this code using "python3 <filename>.py"

